/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entities.UserDetails;
import entities.UserIdentifier;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import model.AuthenticaInterface;
import model.UserDetailsFacade;
import model.UserIdentifierFacade;

/**
 *
 * @author rootmack
 */
@Named(value = "userCalculationsController")
@SessionScoped
public class UserCalculationsController implements Serializable {

 
    @EJB
    private AuthenticaInterface authenticaInterface;;
/**
     * Creates a new instance of UserCalculationsController
     */
    @EJB
    private UserDetailsFacade userDetailsFacade;
    @EJB
    private UserIdentifierFacade userIdentifierFacade;


  
    private UserDetails userDetails = new UserDetails();
    private UserIdentifier userIdentifier = new UserIdentifier();


    private String name;
    private String password;
    private String firstnumber = "";
    private String secondnumber = "";
    private String operator = "";
    private String allcalaculationString = "";
    private List<String> allNames = null;
    private String selectedName = "";
    private List<UserDetails> allSearchedNames = null;

    public String getRadioButtonValue() {
        return radioButtonValue;
    }

    public void setRadioButtonValue(String radioButtonValue) 
    {
        this.radioButtonValue = radioButtonValue;
    }
    private String radioButtonValue = "";

    public UserCalculationsController() {
       // this.authenticaImpl = new AuthenticaImpl();

    }

    public String authenticar() {

        if (authenticaInterface.authenticar(name, password)) {
            //userDetails = authenticaImpl.findUser(name);
            userIdentifier = authenticaInterface.getlogginDetails(name);
            System.out.println("firstSecond:" + (firstnumber + secondnumber));

            if (userIdentifier.getIsAdmin().equals('Y')) {
                return "administratorpage";
            } else {
                return "simplecalculationpage";
            }
        } else {
            printmessage("Wrong password and username combination");
            return null;
        }
    }

    public void calculate() 
    {
        String allcalaculationString;
        boolean isSimpleCalulation;

        if (!firstnumber.isEmpty() && !secondnumber.isEmpty()) 
        {
            allcalaculationString = (firstnumber.concat(operator).concat(secondnumber));
            isSimpleCalulation = true;
        } else 
        {
            allcalaculationString = this.allcalaculationString;
            isSimpleCalulation = false;
        }

        System.out.println(allcalaculationString);

        String results = docomputations(allcalaculationString);
        printmessage(results);

        if (isSimpleCalulation) 
        {
            updateUserData(allcalaculationString, results, "simple");
        } else 
        {
            updateUserData(allcalaculationString, results, "complex");
        }
        clearValues();

      
    }

    public List<UserDetails> findAllearched()
    {
        return allSearchedNames;
    }
    
      public List<UserDetails> findAll() {
        return this.userDetailsFacade.findAll();
    }

    public List<String> findAllnames() {
        List<String> findAllnames = this.userDetailsFacade.findAllnames();
        findAllnames.add(0, "");
        return findAllnames;
    }

    private String docomputations(String calculationsTocompute) 
    {     
        String answer = authenticaInterface.calculate(calculationsTocompute);
        return answer;
    }

    private void printmessage(String message) {
        FacesMessage fm = new FacesMessage(message);
        FacesContext.getCurrentInstance().addMessage("msg", fm);
    }

    private void updateUserData(String calculationRequest, String calulationResponse, String calculationType)
    {
        authenticaInterface.updateUserDetailsl(userIdentifier.getId(),userIdentifier.getName(),calculationType,calulationResponse,calculationRequest);
       
    }

    private void clearValues() {
        firstnumber = "";
        secondnumber = "";
        operator = "";
        allcalaculationString = "";
    }

    public String searchby()
    {        
        Date validDate = isValidDate(selectedName);
        if (validDate!=null)
        {
          allSearchedNames = userDetailsFacade.findbyDate(validDate);
          
        } else 
        {
            allSearchedNames = userDetailsFacade.findNameListbyName(selectedName);
        }
        return "researchresults"; 
    }
    
    
    public Date isValidDate(String value)
    {
        Date parseDate = null;
        try
        {
            DateFormat formatter;
            formatter = new SimpleDateFormat("yyyy/MM/dd");
            parseDate = (Date) formatter.parse(value);

        } catch (ParseException e) 
        {

        }

        return parseDate;
    }

    public UserDetailsFacade getUserCalculationsFacade() {
        return userDetailsFacade;
    }

    public UserDetails getUc() {
        return userDetails;
    }

    public void setUc(UserDetails uc) {
        this.userDetails = uc;
    }

    public UserDetailsFacade getUserDetailsFacade() {
        return userDetailsFacade;
    }

    public void setUserDetailsFacade(UserDetailsFacade userDetailsFacade) {
        this.userDetailsFacade = userDetailsFacade;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;

    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getAllcalaculationString() {
        return allcalaculationString;
    }

    public void setAllcalaculationString(String allcalaculationString) {
        this.allcalaculationString = allcalaculationString;
    }

    public String getFirstnumber() {
        return firstnumber;
    }

    public void setFirstnumber(String firstnumber) {
        this.firstnumber = firstnumber;
    }

    public String getSecondnumber() {
        return secondnumber;
    }

    public void setSecondnumber(String secondnumber) {
        this.secondnumber = secondnumber;
    }

    public List<String> getAllNames() {
        return allNames;
    }

    public void setAllNames(List<String> allNames) {
        this.allNames = allNames;
    }

    public String getSelectedName() {
        return selectedName;
    }

    public void setSelectedName(String selectedName) {
        this.selectedName = selectedName;
    }
    
//       public UserIdentifierFacade getUserIdentifierFacade() {
//        return userIdentifierFacade;
//    }
//
//    public void setUserIdentifierFacade(UserIdentifierFacade userIdentifierFacade) {
//        this.userIdentifierFacade = userIdentifierFacade;
//    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public UserIdentifier getUserIdentifier() {
        return userIdentifier;
    }

    public void setUserIdentifier(UserIdentifier userIdentifier) {
        this.userIdentifier = userIdentifier;
    }

    public List<UserDetails> getAllSearchedNames() {
        return allSearchedNames;
    }

    public void setAllSearchedNames(List<UserDetails> allSearchedNames) {
        this.allSearchedNames = allSearchedNames;
    }

}
