/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entities.UserDetails;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

/**
 *
 * @author rootmack
 */
@Stateless
public class UserDetailsFacade extends AbstractFacade<UserDetails> {

    @PersistenceContext(unitName = "CalculatorOnTheWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() 
    {
        return em;
    }

    public UserDetailsFacade() 
    {
        super(UserDetails.class);
    }

    public List<UserDetails> findbyDate(Date date) {

        List<UserDetails> userDetails = null;
        try {
            userDetails = em.createNamedQuery("UserDetails.findByTransection").setParameter("transection", date, TemporalType.DATE).getResultList();

        } catch (Exception ex) {

        }
        return userDetails;
    }

    public List<UserDetails> findNameListbyName(String name) {
        List<UserDetails> userDetails = null;
        try {
            userDetails = em.createNamedQuery("UserDetails.findByName").setParameter("name", name).getResultList();
        } catch (Exception ex) {

        }
        return userDetails;
    }

    public UserDetails findbyName(String name) {
        UserDetails userDetails = null;
        try {
            userDetails = (UserDetails) em.createNamedQuery("UserDetails.findByName").setParameter("name", name).getSingleResult();
        } catch (Exception ex) {

        }
        return userDetails;
    }

    public List<String> findAllnames() 
    {

        List<String> names = null;
        try {
            names = em.createNamedQuery("UserDetails.findAllNames").getResultList();
        } catch (Exception ex) {

        }

        return names;
    }

}
