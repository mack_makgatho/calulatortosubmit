/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entities.UserIdentifier;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rootmack
 */
@Stateless
public class UserIdentifierFacade extends AbstractFacade<UserIdentifier> {

    @PersistenceContext(unitName = "CalculatorOnTheWeb-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserIdentifierFacade() {
        super(UserIdentifier.class);
    }

    public UserIdentifier findbyName(String name) {
        UserIdentifier userIdentifier = null;
        try {
            userIdentifier = (UserIdentifier) em.createNamedQuery("UserIdentifier.findByName").setParameter("name", name).getSingleResult();
        } catch (Exception ex) {

        }
        return userIdentifier;
    }

    public UserIdentifier findbyName(int id) {
        UserIdentifier userIdentifier = null;
        try {
            userIdentifier = (UserIdentifier) em.createNamedQuery("UserIdentifie.findByName").setParameter("id", id).getSingleResult();
        } catch (Exception ex) {

        }
        return userIdentifier;
    }

    public List<String> findAllnames() {

        List<String> names = null;
        try {
            names = em.createNamedQuery("UserIdentifier.findAll").getResultList();
        } catch (Exception ex) {

        }

        return names;
    }

}
