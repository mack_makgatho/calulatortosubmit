/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entities.UserDetails;
import entities.UserIdentifier;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import utilz.CalculatorUtil;

/**
 *
 * @author rootmack
 */

@Stateless
public class AuthenticaImpl implements AuthenticaInterface
{
   
    @EJB
    public UserDetailsFacade userDetailsFacde;
    @EJB
    public UserIdentifierFacade userIdentifierFacade;
    @EJB
    private CalculatorUtil calculatorUtil;
    UserDetails userDetails = null;
    UserIdentifier userIdentifier=null;

    @Override
    public boolean authenticar(String name, String password) 
    { 
        String username = "";
        String userpassword = "";
        userIdentifier =  getlogginDetails(name);
        
        if(userIdentifier!=null)
        {     
            username = userIdentifier.getName();
            userpassword = userIdentifier.getPassword();
        }
            
            
        return (username.equals(name) && userpassword.equals(password));
    }
    
    public List<String> findallDatabaseNames(List<UserDetails>userDetailsFacdeList) 
    {
        List<String> names = null;
        String name = null;

        for (UserDetails userDetails : userDetailsFacdeList)
        {
            name = userDetails.getName();
            if(!name.isEmpty()&& name!=null)
                 names.add(name);     
        }
        return names;
    }

    @Override
    public UserDetails findUser(String name) 
    {
        UserDetails userDetails = userDetailsFacde.findbyName(name);
       
        return userDetails;
    }



    @Override
    public boolean updatecalculationRequest(String name, String value) 
    {
       boolean valuIsUpdate = false;
       
       UserDetails userToUpdate = findUser(name);
       if(userToUpdate!=null)
       {
           userToUpdate.setCalculationRequest(value);
           valuIsUpdate = true;
       }     
       return valuIsUpdate;
    }

    @Override
    public boolean updatecalculationReponse(String name, String value) 
    {
       boolean valuIsUpdate = false;
       
       UserDetails userToUpdate = findUser(name);
       if(userToUpdate!=null)
       {
           userToUpdate.setCalculationReponse(value);
           valuIsUpdate = true;
       }     
       return valuIsUpdate;
    }

    @Override
    public boolean updateCalculationType(String name, String value)
    {
      boolean valuIsUpdate = false;
       
       UserDetails userToUpdate = findUser(name);
       if(userToUpdate!=null)
       {
           userToUpdate.setCalculationType(value);
           valuIsUpdate = true;
       }     
       return valuIsUpdate;
    }

    @Override
    public String calculate(String math)
    {
      return calculatorUtil.calaculator(math);
    }

    @Override
    public UserIdentifier getlogginDetails(String name)
    {
      
      return  userIdentifierFacade.findbyName(name);
    }

    @Override
    public void updateUserDetailsl(int id, String name, String calculationType, String calculationResponse, String calulationRequest) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        String dateString = dateFormat.format(new Date());

        Date date = null;
        try 
        {
            date = dateFormat.parse(dateString);
        } catch (ParseException ex) 
        {
            Logger.getLogger(AuthenticaImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        UserDetails userDetails = new UserDetails();
        userDetails.setId(id);
        userDetails.setCalculationReponse(calculationResponse);
        userDetails.setCalculationRequest(calulationRequest);
        userDetails.setCalculationType(calculationType);
        userDetails.setName(name);
        userDetails.setTransection(date);
        userDetailsFacde.create(userDetails);
    }

}
