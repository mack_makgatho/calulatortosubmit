/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import entities.UserDetails;
import entities.UserIdentifier;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Remote;

/**
 *
 * @author rootmack
 */
@Remote
public interface AuthenticaInterface 
{
   public boolean authenticar(String name,String password);
   public String calculate(String math);
   public UserDetails findUser(String name);
   public void updateUserDetailsl(int id,String name, String calculationRequest, String calculationResponse, String calculationType);
   public boolean updatecalculationRequest(String name, String value);
   public boolean updatecalculationReponse(String name, String value);
   public boolean updateCalculationType(String name, String value);
   public List<String> findallDatabaseNames(List<UserDetails>userDetaisFacdeList);
   public UserIdentifier getlogginDetails(String name);
   
}
