
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilz;

/**
 *
 * @author rootmack
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.Stateless;
import java.lang.Math.*;

@Stateless
public class CalculatorUtil 
{

    public  Character[] OPERATORS =
    {
        '/', '*', '+', '-'
    };

    public  final String REGEXOPERATORS = "[/+,-,/*,//,-]";
    public  final String REGEXDIGITS = "(\\d+)";

    public  ArrayList<Character> operators = new ArrayList<Character>();
    public  ArrayList<Integer> digits = new ArrayList<Integer>();
    
    
    
    public String calaculator(String math)
    {
        operators.clear();
        digits.clear();
                
        Object answer = "";
        getDigits(math);
        getOperators(math);
        //get highest level sign
        getNextOperator(operators);


        Iterator<Integer> i=digits.iterator();

        while (i.hasNext())
        {
            answer = i.next();
            //System.out.print("answer1:"+String.valueOf(i.next())+' ');       
        }

        System.out.println();


        Iterator<Character> z=operators.iterator();
        while (z.hasNext()) 
        {
            System.out.print(z.next());
            answer = String.valueOf(z.next());
        }
        
        return answer.toString();
    }

    private  void getNextOperator(ArrayList<Character> operators)
    {
        for (Character op : OPERATORS)
        {
           A:
            for (int i = 0; i < operators.size(); i++)
            {

                if (operators.get(i) == '/')
                {
                    operators.remove(i);
                    digits.set(i, (digits.get(i) / digits.get(i + 1)));
                    digits.remove(i + 1);
                    i -= 1;
                    continue A;
                }
            }

            B:
            for (int i = 0; i < operators.size(); i++)
            {

                if (operators.get(i) == '*')
                {
                    System.out.println("if in for");
                    operators.remove(i);
                    digits.set(i, (digits.get(i) * digits.get(i + 1)));
                    digits.remove(i + 1);
                    i -= 1;
                    continue B;
                }
            }
           
            for (int i = 0; i < operators.size(); i++)
            {

                if (operators.get(i) == '+')
                {
                    System.out.println("if in for");
                    operators.remove(i);
                    digits.set(i, (digits.get(i) + digits.get(i + 1)));
                    digits.remove(i + 1);
                    i -= 1;
                    continue;
                }
            }
            
            for (int i = 0; i < operators.size(); i++)
            {

                if (operators.get(i) == '-')
                {
                    operators.remove(i);
                    digits.set(i, (digits.get(i) - digits.get(i + 1)));
                    digits.remove(i + 1);
                    i -= 1;
                    continue;
                }
            }
            
       

        }
    }

    public  void getDigits(String math)
    {
        System.out.println("Getting digits ...");

        Pattern r = Pattern.compile(REGEXDIGITS);
        Matcher m = r.matcher(math);
        while (m.find())
        {
            int t = Integer.parseInt(math.substring(m.start(), m.end()));
            digits.add(t);
        }
        System.out.println("\rFinished getting digits...");
    }

    public  void getOperators(String math)
    {
        System.out.println("Getting Operators..");
        Pattern r = Pattern.compile(REGEXOPERATORS);
        Matcher m = r.matcher(math);
        while (m.find())
        {

            operators.add(math.charAt(m.start()));
        }
        System.out.println("Finished getting Operators..");

    }
}
