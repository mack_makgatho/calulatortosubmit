/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rootmack
 */
@Entity
@Table(name = "user_details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserDetails.findAll", query = "SELECT u FROM UserDetails u")
    , @NamedQuery(name = "UserDetails.findById", query = "SELECT u FROM UserDetails u WHERE u.id = :id")
    , @NamedQuery(name = "UserDetails.findByName", query = "SELECT u FROM UserDetails u WHERE u.name = :name")
    , @NamedQuery(name = "UserDetails.findByCalculationType", query = "SELECT u FROM UserDetails u WHERE u.calculationType = :calculationType")
    , @NamedQuery(name = "UserDetails.findByCalculationRequest", query = "SELECT u FROM UserDetails u WHERE u.calculationRequest = :calculationRequest")
    , @NamedQuery(name = "UserDetails.findByCalculationReponse", query = "SELECT u FROM UserDetails u WHERE u.calculationReponse = :calculationReponse")
    , @NamedQuery(name = "UserDetails.findByTransection", query = "SELECT u FROM UserDetails u WHERE u.transection = :transection")})
public class UserDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    @Size(max = 20)
    @Column(name = "name")
    private String name;
    @Size(max = 20)
    @Column(name = "calculation_type")
    private String calculationType;
    @Size(max = 20)
    @Column(name = "calculation_request")
    private String calculationRequest;
    @Size(max = 20)
    @Column(name = "calculation_reponse")
    private String calculationReponse;
    @Column(name = "transection")
    @Temporal(TemporalType.DATE)
    private Date transection;

    public UserDetails() {
    }

    public UserDetails(Integer id) {
        this.id = id;
    }

    public UserDetails(String name, String calculationType, String calculationRequest, String calculationReponse, Date transection) 
    {
        this.name = name;
        this.calculationType = calculationType;
        this.calculationRequest = calculationRequest;
        this.calculationReponse = calculationReponse;
        this.transection = transection;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(String calculationType) {
        this.calculationType = calculationType;
    }

    public String getCalculationRequest() {
        return calculationRequest;
    }

    public void setCalculationRequest(String calculationRequest) {
        this.calculationRequest = calculationRequest;
    }

    public String getCalculationReponse() {
        return calculationReponse;
    }

    public void setCalculationReponse(String calculationReponse) {
        this.calculationReponse = calculationReponse;
    }

    public Date getTransection() {
        return transection;
    }

    public void setTransection(Date transection) {
        this.transection = transection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserDetails)) {
            return false;
        }
        UserDetails other = (UserDetails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.UserDetails[ id=" + id + " ]";
    }

}
