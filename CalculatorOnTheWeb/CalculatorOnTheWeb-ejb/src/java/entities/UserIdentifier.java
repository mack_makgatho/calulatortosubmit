/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rootmack
 */
@Entity
@Table(name = "user_identifier")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserIdentifier.findAll", query = "SELECT u FROM UserIdentifier u")
    , @NamedQuery(name = "UserIdentifier.findById", query = "SELECT u FROM UserIdentifier u WHERE u.id = :id")
    , @NamedQuery(name = "UserIdentifier.findByName", query = "SELECT u FROM UserIdentifier u WHERE u.name = :name")
    , @NamedQuery(name = "UserIdentifier.findByPassword", query = "SELECT u FROM UserIdentifier u WHERE u.password = :password")
    , @NamedQuery(name = "UserIdentifier.findByIsAdmin", query = "SELECT u FROM UserIdentifier u WHERE u.isAdmin = :isAdmin")})
public class UserIdentifier implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "password")
    private String password;
    @Column(name = "isAdmin")
    private Character isAdmin;

    public UserIdentifier() {
    }

    public UserIdentifier(Integer id) {
        this.id = id;
    }

    public UserIdentifier(Integer id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Character getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Character isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserIdentifier)) {
            return false;
        }
        UserIdentifier other = (UserIdentifier) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.UserIdentifier[ id=" + id + " ]";
    }
    
}
