package entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-20T15:59:09")
@StaticMetamodel(UserDetails.class)
public class UserDetails_ { 

    public static volatile SingularAttribute<UserDetails, String> calculationType;
    public static volatile SingularAttribute<UserDetails, String> calculationRequest;
    public static volatile SingularAttribute<UserDetails, String> calculationReponse;
    public static volatile SingularAttribute<UserDetails, String> name;
    public static volatile SingularAttribute<UserDetails, Integer> id;
    public static volatile SingularAttribute<UserDetails, Date> transection;

}