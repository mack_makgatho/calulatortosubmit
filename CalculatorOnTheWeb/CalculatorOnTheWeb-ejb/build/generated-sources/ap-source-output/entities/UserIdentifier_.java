package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-05-20T15:59:09")
@StaticMetamodel(UserIdentifier.class)
public class UserIdentifier_ { 

    public static volatile SingularAttribute<UserIdentifier, String> password;
    public static volatile SingularAttribute<UserIdentifier, String> name;
    public static volatile SingularAttribute<UserIdentifier, Integer> id;
    public static volatile SingularAttribute<UserIdentifier, Character> isAdmin;

}